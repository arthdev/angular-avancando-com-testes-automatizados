import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActionDirective } from './action.directive';
import { ActionDirectiveModule } from './action.module';

describe(ActionDirective.name, () => {
    let fixture: ComponentFixture<ActionDirectiveTestComponent> = null;
    let component: ActionDirectiveTestComponent = null;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [ActionDirectiveModule],
            declarations: [ActionDirectiveTestComponent]
            // providers: [],
        }).compileComponents();

        fixture = TestBed.createComponent(ActionDirectiveTestComponent);
        component = fixture.componentInstance;        
    });

    it(`@Output appAction should emit event with payload when ENTER key is pressed`,
        () => {
            const div = <HTMLElement> fixture.nativeElement.querySelector('.dummy-component');
            const event = new KeyboardEvent('keyup', { key: 'Enter' });
            div.dispatchEvent(event);
            expect(component.hasEvent()).toBeTrue();
        });

    it(`@Output appAction should emit event with payload when clicked`,
        () => {
            const div = <HTMLElement> fixture.nativeElement.querySelector('.dummy-component');
            const event = new Event('click');
            div.dispatchEvent(event);
            expect(component.hasEvent()).toBeTrue();
        });
});

@Component({
    template: `<div class="dummy-component" (appAction)="actionHandler($event)"></div>`
})
class ActionDirectiveTestComponent {

    private event: Event = null;
    
    actionHandler(event: Event) {
        this.event = event;
    }

    hasEvent() {
        return !!this.event;
    }
}