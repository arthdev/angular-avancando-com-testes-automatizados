import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-photo-frame',
    templateUrl: './photo-frame.component.html',
    styleUrls: ['./photo-frame.component.scss'],
})
export class PhotoFrameComponent implements OnInit, OnDestroy {    
    @Output() liked: EventEmitter<void> = new EventEmitter<void>();
    @Input() description = '';
    @Input() src = '';
    @Input() likes = 0;
    private debounceSubject: Subject<void> = new Subject();
    private unsubcribe: Subject<void> = new Subject();

    ngOnInit(): void {
        this.debounceSubject
            .asObservable()
            .pipe(debounceTime(500), takeUntil(this.unsubcribe))
            .subscribe(() => this.liked.emit());
    }

    ngOnDestroy(): void {
        this.unsubcribe.next();
        this.unsubcribe.complete();
    }

    like(): void {
        this.debounceSubject.next();
    }
}