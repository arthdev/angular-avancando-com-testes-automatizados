import { TestBed } from '@angular/core/testing';
import { PhotoBoardService } from './photo-board.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

const mockData = {
    api: 'http://localhost:3000/photos',
    data: [
        {
            id: 1,
            description: 'example 1',
            src: '',
        },
        {
            id: 2,
            description: 'example 2',
            src: '',
        },
    ]
};

describe(PhotoBoardService.name, () => {
    let service: PhotoBoardService;
    let httpController: HttpTestingController;
   beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [PhotoBoardService],
        })
        .compileComponents();

        service = TestBed.inject(PhotoBoardService);
        httpController = TestBed.inject(HttpTestingController);
   }); 

   // httpController.verify(): 
   // verifica se alguma request foi feita e não foi mockada até o final de cada test case.
   // útil caso o desenvolvedor esqueça de mockar alguma request.
   afterEach(() => httpController.verify());

   it(`${PhotoBoardService.prototype.getPhotos.name}, should return photo with description in uppercase`,
    (done) => {            
        service.getPhotos()
            .subscribe((photos) => {
                expect(photos[0].description).toBe(mockData.data[0].description.toUpperCase());
                expect(photos[1].description).toBe(mockData.data[1].description.toUpperCase());
                done();
            });
        // importante: o mock da request deve ser feito depois da chamada que envia a request    
        httpController.expectOne(mockData.api)
            .flush(mockData.data);
    });
});