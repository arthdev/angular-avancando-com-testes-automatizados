import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Photo } from '../interfaces/photo';
import { PhotoBoardService } from './photo-board.service';

function buildPhotosList(): Photo[] {
    const photos: Photo[] = [];

    for (let index = 0; index < 8; index++) {
        photos.push({
            id: index + 1,
            url: '',
            description: '',
        });          
    }

    return photos;
}

@Injectable()
export class PhotoBoardMockService extends PhotoBoardService {
    getPhotos() {
        return of(buildPhotosList());
    }
}