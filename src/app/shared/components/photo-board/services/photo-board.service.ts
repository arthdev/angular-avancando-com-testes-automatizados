import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay, map } from 'rxjs/operators';
import { Photo } from '../interfaces/photo';

@Injectable()
export class PhotoBoardService {
    constructor(private readonly http: HttpClient) {
        
    }

    getPhotos() {
        return this.http.get<Photo[]>('http://localhost:3000/photos')
            .pipe(
                delay(2000),
                map((photos) => {
                    return photos.map(photo => ({ ...photo, description: photo.description.toUpperCase()}));
                })
            );
    }
}