import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Photo } from './interfaces/photo';

@Component({
    selector: 'app-photo-board',
    templateUrl: './photo-board.component.html',
    styleUrls: ['./photo-board.component.scss'],
})
export class PhotoBoardComponent implements OnChanges {

    @Input() public photos: Photo[];

    public rows: any[][] = [];

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['photos'] && changes['photos'].currentValue) {
            this.rows = this.groupColumns(changes['photos'].currentValue);
        }
    }

    groupColumns(photos: Photo[]): any[][] {
        const newRows = [];
        const columnsPerRow = 4;

        for (let index = 0; index < photos.length; index += columnsPerRow) {
            newRows.push(photos.slice(index, index + 4));
        }

        return newRows;
    }
}