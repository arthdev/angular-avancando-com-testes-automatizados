import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Photo } from 'src/app/shared/components/photo-board/interfaces/photo';
import { PhotoBoardMockService } from 'src/app/shared/components/photo-board/services/photo-board.mock.service';
import { PhotoBoardService } from 'src/app/shared/components/photo-board/services/photo-board.service';
import { PhotoListComponent } from './photo-list.component';
import { PhotoListModule } from './photo-list.module';

describe(PhotoListComponent.name + ' Mock Provider', () => {
    let fixture: ComponentFixture<PhotoListComponent>;
    let component: PhotoListComponent;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [PhotoListModule, HttpClientModule],
            providers: [
                {
                    provide: PhotoBoardService,
                    // Opção 1: Pode registrar o mock provider como um objeto anônimo
                    // useValue: {
                    //     getPhotos() {
                    //         return of(buildPhotosList())
                    //     }
                    // }
                    // Opção 2: Caso deseje manter algum comportamento do serviço original,
                    // pode criar uma classe que extenda ele e altere só o comportamento desejado.
                    useClass: PhotoBoardMockService,
                }
            ],
        })
        .compileComponents();

        fixture = TestBed.createComponent(PhotoListComponent);
        component = fixture.componentInstance;
    });

    it(`Should create componente`, () => {
        expect(component).toBeTruthy();
    });

    it(`Should display board when data arrives`, () => {
        fixture.detectChanges();
        const board = fixture.nativeElement.querySelector('app-photo-board');
        const loader = fixture.nativeElement.querySelector('.loader');

        expect(board).not.toBeNull();
        expect(loader).toBeNull();
    });
});