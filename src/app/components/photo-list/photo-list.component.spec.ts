import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Photo } from 'src/app/shared/components/photo-board/interfaces/photo';
import { PhotoBoardService } from 'src/app/shared/components/photo-board/services/photo-board.service';
import { PhotoListComponent } from './photo-list.component';
import { PhotoListModule } from './photo-list.module';

function buildPhotosList(): Photo[] {
    const photos: Photo[] = [];

    for (let index = 0; index < 8; index++) {
        photos.push({
            id: index + 1,
            url: '',
            description: '',
        });          
    }

    return photos;
}


describe(PhotoListComponent.name, () => {
    let fixture: ComponentFixture<PhotoListComponent>;
    let component: PhotoListComponent;
    let service: PhotoBoardService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [PhotoListModule, HttpClientModule],
            // providers: [],
        })
        .compileComponents();

        fixture = TestBed.createComponent(PhotoListComponent);
        component = fixture.componentInstance;
        service = TestBed.inject(PhotoBoardService);
    });

    it(`Should create componente`, () => {
        expect(component).toBeTruthy();
    });

    it(`Should display board when data arrives`, () => {
        const photos = buildPhotosList();
        spyOn(service, 'getPhotos').and.returnValue(of(photos));
        fixture.detectChanges();
        const board = fixture.nativeElement.querySelector('app-photo-board');
        const loader = fixture.nativeElement.querySelector('.loader');

        expect(board).not.toBeNull();
        expect(loader).toBeNull();
    });

    it(`Should display board while waiting for the data`, () => {
        spyOn(service, 'getPhotos').and.returnValue(null);
        fixture.detectChanges();
        const board = fixture.nativeElement.querySelector('app-photo-board');
        const loader = fixture.nativeElement.querySelector('.loader');

        expect(board).toBeNull();
        expect(loader).not.toBeNull();
    });
});