# Curso Alura

## Componentes "Fake"/"Fantoche" para testar directives.
Para testar directives, é necesário utilizar um component dummy/fantoche/fake. Exemplo no teste da app action directive.

## fixture.debugElement vs. fixture.nativeElement
- os dois são o mesmo elemento
- fixture.nativeElement é um atalho para acessar fixture.debugElement.nativeElement

## ngOnchanges deve ser chamado manualmente nos testes.
O detectChanges() não chama ele.
Ele só é chamado quando tem um input binding ativo. Pode ser usada a estratégia do dummy component, mas não é recomendada.
Exemplo no teste do photo board component.

## async pipe com ngIf
Deve ser criado um alias (photos$ as photos) para não repetir a subscription. 
Exemplo no photo list component.

## testar service que chama back end com http client
É indicado fazer quando o service faz alguma transformação nos dados que vêm do back end.
Exemplo no teste do photo board service.

## mock provider
Recomendado quando os métodos de uma dependência devem se comportar da mesma forma em todos os test cases.
Em vez de faz o spyOn no método em cada teste, registra-se o serviço mockado nos providers do modulo de teste.
Exemplo em photo list component mock provider